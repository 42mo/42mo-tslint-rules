const fs = require('fs');
const rootPath = '../../..';
const tsLintFilePath = `${rootPath}/tslint.json`;
const timeOut = setTimeout(() => {
  console.info(' Time is up!!!');
  process.exit();
}, 30 * 1000);
let installTslintRules = true;


const cl = require('readline').createInterface(process.stdin, process.stdout);
const question = function (q) {
  return new Promise((res, rej) => {
    cl.question(q, answer => {
      res(answer);
    })
  });
};

try {
  let projetctTslint = null;
  const myRules = JSON.parse(fs.readFileSync('./42mo-tslint.json'));

  if (fs.existsSync(tsLintFilePath)) {
    projetctTslint = JSON.parse(fs.readFileSync(tsLintFilePath));
    if (JSON.stringify(myRules.rules) === JSON.stringify(projetctTslint.rules)) {
      installTslintRules = false;
      console.info('42mo-tslint rules were already set!');
      clearTimeout(timeOut);
      process.exit();
    }
  } else {
    console.error('No tsling.json file found in Your project root folder!');
  }

  if (projetctTslint && installTslintRules) {
    (async function main() {
      const userAnswers = ['yes', 'y', 'no', 'n'];
      let answer = '';
      while (userAnswers.indexOf(answer.toLowerCase()) === -1) {
        answer = await question('Do You want to modify Your tslint.json by 42mo-tslint rules? (Y/N): ');
      }
      clearTimeout(timeOut);
      if (answer === 'yes' || answer === 'y') {
        projetctTslint.rules = myRules.rules;
        fs.writeFileSync(tsLintFilePath, JSON.stringify(projetctTslint,null,'  '), {encoding: 'utf8', flag: 'w'});
        console.info('Your tslint were modified with 42mo-tslint rules');
        process.exit();
      } else {
        console.info('Your tslint were not modified !');
        process.exit();
      }
    })();

  }
} catch (err) {
  console.error('error', err);
}

